﻿using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAcces
{
    public class InventaryContext : DbContext
    {
        public DbSet<CategoryEntity> tbl_categories { get; set; }
        public DbSet<InOutEntity> tbl_inOuts { get; set; }
        public DbSet<ProductEntity> tbl_products { get; set; }
        public DbSet<StorageEntity> tbl_storages { get; set; }
        public DbSet<WareHouseEntity> tbl_wareHouse { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if(!options.IsConfigured)
            {
                options.UseSqlServer("Server=DESKTOP-T4V0DON; Database=InventoryDb; User Id= sa; Password=Juan170922$");
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CategoryEntity>().HasData(
                new CategoryEntity { CategoryId= "ASH", CategoryName="Aseo Hogar"},
                  new CategoryEntity { CategoryId = "ASP", CategoryName = "Aseo Personal" },
                    new CategoryEntity { CategoryId = "HGR", CategoryName = " Hogar" },
                      new CategoryEntity { CategoryId = "PRF", CategoryName = " Perfumería" },
                        new CategoryEntity { CategoryId = "SLD", CategoryName = "Salud" },
                          new CategoryEntity { CategoryId = "VDJ", CategoryName = " Video Juegos" },
                            new CategoryEntity { CategoryId = "ELC", CategoryName = "Electrodomesticos" },
                              new CategoryEntity { CategoryId = "CSL", CategoryName = "Consolas" },
                                new CategoryEntity { CategoryId = "PRG", CategoryName = "Programas" },
                                  new CategoryEntity { CategoryId = "DPR", CategoryName = "Deportes" }

                );
            modelBuilder.Entity<WareHouseEntity>().HasData(
                new WareHouseEntity { WareHouseId=Guid.NewGuid().ToString(), WareHouseName="Bodega Central",WareHouseAddress="Calle 65 Sur #72-65" },
                new WareHouseEntity { WareHouseId = Guid.NewGuid().ToString(), WareHouseName = "Bodega Sur", WareHouseAddress = "Calle 81 Sur #59115, Torre 2, 205" },
                new WareHouseEntity { WareHouseId = Guid.NewGuid().ToString(), WareHouseName = "Bodega Norte", WareHouseAddress = "Calle 32 Sur #14-65" }
                );
        }
    }
}
