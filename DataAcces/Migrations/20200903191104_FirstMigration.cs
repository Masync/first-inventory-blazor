﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAcces.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbl_categories",
                columns: table => new
                {
                    CategoryId = table.Column<string>(maxLength: 50, nullable: false),
                    CategoryName = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_categories", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "tbl_wareHouse",
                columns: table => new
                {
                    WareHouseId = table.Column<string>(maxLength: 50, nullable: false),
                    WareHouseName = table.Column<string>(maxLength: 100, nullable: false),
                    WareHouseAddress = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_wareHouse", x => x.WareHouseId);
                });

            migrationBuilder.CreateTable(
                name: "tbl_products",
                columns: table => new
                {
                    ProductId = table.Column<string>(maxLength: 10, nullable: false),
                    ProductName = table.Column<string>(maxLength: 100, nullable: false),
                    ProductDescription = table.Column<string>(maxLength: 600, nullable: true),
                    TotalQuantity = table.Column<int>(nullable: false),
                    CategoryId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_products", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_tbl_products_tbl_categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "tbl_categories",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbl_storages",
                columns: table => new
                {
                    StorageId = table.Column<string>(maxLength: 50, nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    PartialQuantity = table.Column<int>(nullable: false),
                    ProductId = table.Column<string>(nullable: true),
                    WareHouseId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_storages", x => x.StorageId);
                    table.ForeignKey(
                        name: "FK_tbl_storages_tbl_products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "tbl_products",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_tbl_storages_tbl_wareHouse_WareHouseId",
                        column: x => x.WareHouseId,
                        principalTable: "tbl_wareHouse",
                        principalColumn: "WareHouseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbl_inOuts",
                columns: table => new
                {
                    InOutId = table.Column<string>(maxLength: 50, nullable: false),
                    InOutDate = table.Column<DateTime>(nullable: false),
                    Quantity = table.Column<int>(maxLength: 900, nullable: false),
                    IsInput = table.Column<bool>(nullable: false),
                    StorageId = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbl_inOuts", x => x.InOutId);
                    table.ForeignKey(
                        name: "FK_tbl_inOuts_tbl_storages_StorageId",
                        column: x => x.StorageId,
                        principalTable: "tbl_storages",
                        principalColumn: "StorageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tbl_inOuts_StorageId",
                table: "tbl_inOuts",
                column: "StorageId");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_products_CategoryId",
                table: "tbl_products",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_storages_ProductId",
                table: "tbl_storages",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_tbl_storages_WareHouseId",
                table: "tbl_storages",
                column: "WareHouseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbl_inOuts");

            migrationBuilder.DropTable(
                name: "tbl_storages");

            migrationBuilder.DropTable(
                name: "tbl_products");

            migrationBuilder.DropTable(
                name: "tbl_wareHouse");

            migrationBuilder.DropTable(
                name: "tbl_categories");
        }
    }
}
