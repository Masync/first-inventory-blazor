﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAcces.Migrations
{
    public partial class AddData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "tbl_categories",
                columns: new[] { "CategoryId", "CategoryName" },
                values: new object[,]
                {
                    { "ASH", "Aseo Hogar" },
                    { "ASP", "Aseo Personal" },
                    { "HGR", " Hogar" },
                    { "PRF", " Perfumería" },
                    { "SLD", "Salud" },
                    { "VDJ", " Video Juegos" },
                    { "ELC", "Electrodomesticos" },
                    { "CSL", "Consolas" },
                    { "PRG", "Programas" },
                    { "DPR", "Deportes" }
                });

            migrationBuilder.InsertData(
                table: "tbl_wareHouse",
                columns: new[] { "WareHouseId", "WareHouseAddress", "WareHouseName" },
                values: new object[,]
                {
                    { "26be3db8-6daa-4019-8b73-88e0c708c8e3", "Calle 65 Sur #72-65", "Bodega Central" },
                    { "ede58f1c-bbe8-400f-bc51-68f51af0fd40", "Calle 81 Sur #59115, Torre 2, 205", "Bodega Sur" },
                    { "d350a28b-71bb-428d-bd2c-5005dde0d277", "Calle 32 Sur #14-65", "Bodega Norte" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "ASH");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "ASP");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "CSL");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "DPR");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "ELC");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "HGR");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "PRF");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "PRG");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "SLD");

            migrationBuilder.DeleteData(
                table: "tbl_categories",
                keyColumn: "CategoryId",
                keyValue: "VDJ");

            migrationBuilder.DeleteData(
                table: "tbl_wareHouse",
                keyColumn: "WareHouseId",
                keyValue: "26be3db8-6daa-4019-8b73-88e0c708c8e3");

            migrationBuilder.DeleteData(
                table: "tbl_wareHouse",
                keyColumn: "WareHouseId",
                keyValue: "d350a28b-71bb-428d-bd2c-5005dde0d277");

            migrationBuilder.DeleteData(
                table: "tbl_wareHouse",
                keyColumn: "WareHouseId",
                keyValue: "ede58f1c-bbe8-400f-bc51-68f51af0fd40");
        }
    }
}
