﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities
{
    // Categorias de productos 
    public class CategoryEntity
    {
        [Key] // Llave primaria
        [StringLength(50)]  // Tamaño maximo
        public string CategoryId { get; set; }
        [Required]
        [StringLength(100)]
        public string CategoryName { get; set; }

        //Relacion Con Productos (ProductEntity)
        public ICollection<ProductEntity> Products { get; set; }

    }
}
