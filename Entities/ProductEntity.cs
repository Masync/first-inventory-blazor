﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Text;

namespace Entities
{
    public class ProductEntity
    {
        [Key] // Llave primaria
        [StringLength(10)] // Tamaño maximo
        public string ProductId { get; set; }
        [Required]
        [StringLength(100)]
        public string ProductName { get; set; }
        [StringLength(600)]
        public string ProductDescription { get; set; }

        public int TotalQuantity { get; set; }

        //Relacion con Categorias (CategoryEntity)
        public string CategoryId { get; set; }
        public CategoryEntity Category { get; set; }

        //Relacion con Almacenamiento (StorageEntity)
        public ICollection<StorageEntity> Storages { get; set; }

    }
}
