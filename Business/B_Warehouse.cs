﻿using DataAcces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class B_Warehouse
    {
        public List<WareHouseEntity> WarehouseList()
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_wareHouse.ToList();
            }
        }
        public void AddWarehouse(WareHouseEntity OwareHouseEntity)
        {
            using(InventaryContext db = new InventaryContext())
            {
                db.tbl_wareHouse.Add(OwareHouseEntity);
            }
        }
        public void UpdateWarehouse(WareHouseEntity OwareHouseEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_wareHouse.Update(OwareHouseEntity);
            }
        }
    }
}
