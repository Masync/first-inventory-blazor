﻿using DataAcces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class B_Category
    {
        public static List<CategoryEntity> CategoryList()
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_categories.ToList();
            }
        }
        public static CategoryEntity categorytById(string id)
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_categories.ToList().LastOrDefault(p => p.CategoryId == id);
            }
        }
        public static void CreateCategory(CategoryEntity OcategoryEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_categories.Add(OcategoryEntity);
                db.SaveChanges();
            }
        }
        public static void UpdateCategory(CategoryEntity OcategoryEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_categories.Update(OcategoryEntity);
                db.SaveChanges();
            }
        }
    }
}
