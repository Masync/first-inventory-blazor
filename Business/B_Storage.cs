﻿using DataAcces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class B_Storage
    {
        public List<StorageEntity> StorageList()
        {
            using(InventaryContext db = new InventaryContext())
            {
                return db.tbl_storages.ToList();
            }
        }
        public void AddStorage (StorageEntity OstorageEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_storages.Add(OstorageEntity);
                db.SaveChanges();
            }
        }
        public void UpdateStorage (StorageEntity OstorageEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_storages.Update(OstorageEntity);
                db.SaveChanges();
            }
        }
    }
}
