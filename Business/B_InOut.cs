﻿using DataAcces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class B_InOut
    {
        public List<InOutEntity> InOutList()
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_inOuts.ToList();
            }
        }
        public void AddInOuts(InOutEntity OinOutEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_inOuts.Add(OinOutEntity);
                db.SaveChanges();
            }
        }
        public void UpdateInOuts(InOutEntity OinOutEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_inOuts.Update(OinOutEntity);
                db.SaveChanges();
            }
        }
    }
}
