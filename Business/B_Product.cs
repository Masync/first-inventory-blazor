﻿using DataAcces;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    public class B_Product
    {
        public static List<ProductEntity> ProductList()
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_products.ToList();
            }
        }
        public static ProductEntity ProductById(string id)
        {
            using (InventaryContext db = new InventaryContext())
            {
                return db.tbl_products.ToList().LastOrDefault(p => p.ProductId == id);
            }
        }
        public static void AddProduct(ProductEntity OproductEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_products.Add(OproductEntity);
                db.SaveChanges();
            }
        }
        public static void UpdateProduct (ProductEntity OproductEntity)
        {
            using (InventaryContext db = new InventaryContext())
            {
                db.tbl_products.Update(OproductEntity);
                db.SaveChanges();
            }
        }
    }
}
